#!/usr/bin/env bash

byobu-tmux new -d -s ruststudy
byobu-tmux send-keys -t ruststudy.0 "byobu-tmux rename-window idiomsrepo && ls -la " ENTER
byobu-tmux new-window -n 'rustrepo'
byobu-tmux new-window -n 'workarea'
byobu-tmux new-window -n 'runner'
byobu-tmux a -t ruststudy

