fn main() {
    let s1 = String::from("hello");

    let len = calculate_length(&s1);

    println!("The length of '{}' is {}.", s1, len);

    let mut s = String::from("hello");
    change(&mut s);
    println!("new val ?{}", s);

    let mut s = String::from("try two ");
    println!("initial | {:?}", s);

    let y = &mut s;
    y.push_str(" michel");
    println!("change using y | {:?}", y);

    let w = &s;
    println!("final ? |{:?}", w);

}

fn calculate_length(s: &String) -> usize {
    s.len()
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}