fn main() {
    println!("Hello, world!");

    let number = 3;

    if number < 5 {
        println!("condition was true");
    } else {
        println!("condition was false");
    }

    let number = 6;

    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }

    let cndtn = true;
    let number = if cndtn { 5 } else { 6 };

    println!("The value of number is: {}", number);

    // be carefull cowboy let number = if condition { 5 } else { "6" };

    println!("The value of number is: {}", number);

    play_loop_1();
    play_loop_2();
    play_loop_3();
    play_loop_4();
    play_loop_5();
    play_loop_6();
}

fn play_loop_1(){
	let mut count = 0;
    'counting_up: loop {
        println!("count = {}", count);
        let mut remain = 10;

        loop {
            println!("remaining = {}", remain);
            if remain == 9 {
                break;
            }
            if count == 2 {
                break 'counting_up;
            }
            remain -= 1;
        }

        count += 1;
    }
    println!("End count = {}", count);
}

fn play_loop_2() {
	let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {}", result);
}

fn play_loop_3(){
	let mut number = 3;

    while number != 0 {
        println!("{}!", number);

        number -= 1;
    }

    println!("LIFTOFF!!!");
}

fn play_loop_4(){
	let a = [10, 20, 30, 40, 50];
    let (mut index , len) = (0, a.len());

    while index < len {
        println!("the value is: {}", a[index]);
        index += 1;
    }
}

fn play_loop_5(){
	let a = [10, 20, 30, 40, 50];
	// it is not error prone, taken of the book.
    for element in a {
        println!("the value is: {}", element);
    }
}

fn play_loop_6(){
	// pythonic ?? interesting combination of ideas in the design ?
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}

