use std::fs;
use std::error::Error;

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    /*
        For now, just know that Box<dyn Error> means the function
        will return a type that implements the Error trait, but
        we don’t have to specify what particular type the return
        value will be.

        This gives us flexibility to return error values that may
        be of different types in different error cases. The dyn
        keyword is short for “dynamic.”
    */
    let contents = fs::read_to_string(config.filename.clone())?;

    println!("Searching: {:?}", config.query);
    println!("With text:\n{:?}", contents);

    let lineswithquery = search(&config.query, &contents);
    if lineswithquery.len() != 0 {
        println!("Lines with query = {:?} are:", config.query);
        for line in lineswithquery {
            println!("{}", line);
        }
    } else {
        println!("In the file {:?} not lines with query {:?} were found.",
            config.filename, config.query.clone()
        );
    }

    Ok(())
}

pub struct Config {
    pub query: String,
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 3 {
            return Err("not enough arguments");
        }

        let query = args[1].clone();
        let filename = args[2].clone();

        Ok(Config { query, filename })
    }
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();

    for line in contents.lines() {
        if line.contains(query) {
            results.push(line);
        }
    }

    results
}
