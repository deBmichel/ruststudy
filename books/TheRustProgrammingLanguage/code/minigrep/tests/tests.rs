use minigrep::*;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn one_result() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(vec!["safe, fast, productive."], minigrep::search(query, contents));
    }

    #[test]
    fn michel() {
        let query = "michel";
        let contents = "\
Rust: michel
safe, michel
fast, michel
productive. michel
Pick three. michel";
        let expected = vec![
            "Rust: michel",
            "safe, michel",
            "fast, michel",
            "productive. michel",
            "Pick three. michel",
        ];
        assert_eq!(expected, minigrep::search(query, contents));
    }

    #[test]
    fn fail_michel() {
        let query = "michel";
        let contents = "\
Rust: michel
safe, michel
fast, michel
productive. michel
Pick three. michel";
        let expected = vec![
            "Rust: michel",
            "safey, michel",
            "fasty, michel",
            "productivee. michel",
            "Pick threey. michel",
        ];
        assert_eq!(expected, minigrep::search(query, contents));
    }

}
