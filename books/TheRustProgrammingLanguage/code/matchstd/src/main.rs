#[derive(Debug)] // so we can inspect the state in a minute
enum UsState {
    Alabama,
    Alaska,
    // --snip--
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

#[derive(Debug)]
enum Result {
	Greater,
	Minor,
	Equal,
}

fn validator (x: u32, y: u32) -> Result {
	if x > y {
		Result::Greater
	} else if x < y {
		Result::Minor
	} else {
		Result::Equal
	}
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        }
    }
}

fn val_reviewer(val: u32) {
	match val {
		// Only the first MATCH review the answer when function takes the val 4.
		val if val % 2 == 0 => {
			println!("{:?} => Related to TWO...", val);
		},
		val if val % 3 == 0 => {
			println!("{:?} => Related to THREE...", val);
		},
		val if val % 4 == 0 => {
			println!("{:?} => Related to FOUR...", val);
		},
		_ => {
			println!("{:?} => WITHOUT MATCH", val);
		},
	}
}

fn match_tuple (t : (&str, &str)) {
	match t {
		("fruit", "low") => {println!("fruit, low");},
		("fruit", "hight") => {println!("fruit, hight");},
		("water", "low") => {println!("water, low");},
		("water", "hight") => {println!("water, hight");},
		_ => {println!("WE DONT HAVE MATCH");},
	}
}

fn printer (val: u32) {
    println!("{:?}", val)
}

fn main() {
    println!("Playing with match ..");
    println!("val = {:?}", value_in_cents(Coin::Quarter(UsState::Alaska)));
    match validator(3, 5) {
    	Result::Greater => {
    		println!("{:?}", Result::Greater);
    	},
    	Result::Minor => {
    		println!("{:?}", Result::Minor);
    	},
    	Result::Equal => {
    		println!("{:?}", Result::Equal);
    	},
    }

    let x = [1,2,3,4,5,6,7,8, 9,10,11];

    for val in x {
		val_reviewer(val);
    }

    let t = 7;
    let c = t;
    println!("{:?}", t);

    let x = String::from("sss");
    // let t = x; ownership , SURE
	println!("{:?}", x);    

	let alfa = [("fruit", "low"), ("fruit", "hight"), ("water", "low"), ("water", "hight"), ("","")];
	for t in alfa{
		match_tuple(t);
		println!("{:?}", t);
	}

}
