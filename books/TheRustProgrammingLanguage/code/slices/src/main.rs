fn main() {
    println!("Hello, world!");
    let str_rev = String::from("Michel M S");
    let i = first_word(&str_rev);
    let fword = first_word_t(&str_rev);
    let opt = option_str_b(&str_rev);
    println!("{:?}, {:?}, {:?}", i, fword, opt);


let a = [1, 2, 3, 4, 5];

let slice = &a[1..3];

assert_eq!(slice, &[2, 3, 7]);
}

fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    s.len()
}

fn first_word_t(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}

/*fn option_str_b()-> &str { fails by borrowed
	let str_rev = String::from("Michel M S");
	return &str_rev[..5];
}*/

fn option_str_b(str_rev: &String)-> &str {
	return &str_rev[..6];
}

// Test using &i32 <- () AND &i32 <- (&I32)



