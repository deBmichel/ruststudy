package main

func give() (int, int, int) {
	return 1, 2, 3
}

func receive(a, b, c int) {
	println("receive", a, b, c)
}

func send(x, y, z int) {
	println("send", x, y, z)
}

func main() {
	receive(give())
	send(give())
}
