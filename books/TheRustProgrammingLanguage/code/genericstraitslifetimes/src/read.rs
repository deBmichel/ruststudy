fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

fn give (v: i32) -> (i32, i32, i32) {
	(v*2, v/3, v+5)
}

fn receive((a, b, c) : (i32, i32, i32)) {
	println!("{:?} , {:?} , {:?}", a,b,c);
}

fn send((x, y, z) : (i32, i32, i32)) {
	println!("{:?} , {:?} , {:?}", x,y,z);
}

fn main () {
	receive(give(3));
	send(give(5));
}
