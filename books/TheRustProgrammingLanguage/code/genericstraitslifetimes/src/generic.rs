/*
	Trying to mix using generic types in vector and points.

	Run using:
		rustc -o targetmichelexe generic.rs && ./targetmichelexe && rm targetmichelexe

		&& rm generic.rs

	I was playing with elemental Rust datatypes.
	And triying sometype of Reflection?.
	Thinking in GOlang ?
*/

#[derive(Debug, Clone)]
enum Generic {
	Number(TypeNumber),
	CharStr(TypeCharStr), 
	// To solve the problem I received help of @Svetlin Stefanov
	// Important lesson I have to read more Rust documentation
	// sure.
}

#[derive(Debug, Clone)]
enum TypeNumber {
	ItF32(f32),
	ItF64(f64),
	ItI8(i8),
	ItI16(i16),
	ItI32(i32),
	ItI64(i64),
	ItU8(u8),
	ItU16(u16),
	ItU32(u32),
	ItU64(u64),
}

#[derive(Debug, Clone)] // A lesson here, clone may be a common trait.
enum TypeCharStr {
	// ItChar can be in TypeNumber but it is not the idea.
	ItChar(char),
	ItString(String),
	ItStr(Box<str>),
}

#[derive(Debug, Copy, Clone)]
struct Point<Generic> {
    x: Generic,
    y: Generic,
}

struct All <T> {
	val: T, 
}

impl Point <Generic> {
    fn mix_samex_useyfromarg(self, other: &Point<Generic>) -> Point<Generic> {
	   	Point {
	        x: self.x,
	        y: other.y.clone(),
	    }
	}

    fn printpoint(self){
    	println!("Point {{x: {:?}, y:{:?}}}", self.x, self.y);
    }
}

fn data() -> (Vec<Generic>, usize) {
	// I can have a vector with different basic datatypes.
	// Generic can be functional but you need to learn 
	// about traits, lesson of a great contact.
	let values : Vec<Generic> = Vec::from (
		[
			Generic::Number(TypeNumber::ItF32(2 as f32)),
			Generic::Number(TypeNumber::ItF64(3 as f64)),
			Generic::Number(TypeNumber::ItI32(5 as i32)),
			Generic::Number(TypeNumber::ItI64(7 as i64)),
			Generic::Number(TypeNumber::ItI8(11 as i8)),
			Generic::Number(TypeNumber::ItI16(13 as i16)),
			Generic::Number(TypeNumber::ItI32(17 as i32)),
			Generic::Number(TypeNumber::ItI64(19 as i64)),
			Generic::Number(TypeNumber::ItU8(23 as u8)),
			Generic::Number(TypeNumber::ItU16(29 as u16)),
			Generic::Number(TypeNumber::ItU32(31 as u32)),
			Generic::Number(TypeNumber::ItU64(37 as u64)),
			Generic::CharStr(TypeCharStr::ItString(String::from("Michel"))),
			Generic::CharStr(TypeCharStr::ItString(String::from("Test"))),
			Generic::CharStr(TypeCharStr::ItChar('M')),
			Generic::CharStr(TypeCharStr::ItStr(Box::from("Michel"))),
		]
	);
	let len_values = values.len();
	// beautifull lesson, in the return you can have borrows
	// be carefull
	(values, len_values)
}

fn trying_mix((bases, len_bases) : (Vec<Generic>, usize)){
	let mut cases : Vec <Point<Generic>> = Vec::new();
	for i in 0..len_bases {
		for j in 0..len_bases {
			if i != j {
				cases.push(
					Point { 
						x: bases[i].clone(),
						y: bases[j].clone(),
					}
				);
			}
		}
	}
	let len_cases = cases.len();

	for i in 0..len_cases {
		for j in 0..len_cases {
			if i != j {
				cases[i].clone().printpoint();
				cases[j].clone().printpoint();
				println!(
					"The mix was : {:?}", 
					cases[i].clone().mix_samex_useyfromarg(&cases[j])
				);
			}
		}
	}
}

/*
	I was thinking how to return all type and searching
	in internet I found std::any::type_name::<T>() then 
	I decided to do a minor combination ; sure :)

	print_type_of(return_different_type(float, int, string))

	the combination really function, and then the next 
	question is:  how to use in convert ?.

	I can use : something like this ?
	https://stackoverflow.com/questions/63967743/what-is-the-correct-syntax-to-return-a-function-in-rust
*/
fn print_type_of<T>(s: &T){
    println!("{}", std::any::type_name::<T>());
}

fn return_different_type<T>(s: &T) -> &T{
	&s
}

fn convert_number(toconvert : TypeNumber){
	match toconvert {
		TypeNumber::ItF32(toconvert) => {
			println!("It is ItF32 {:?}", toconvert);
		}
		TypeNumber::ItF64(toconvert) => {
			println!("It is ItF64 {:?}", toconvert);
		}
		TypeNumber::ItI8(toconvert) => {
			println!("It is ItI8 {:?}", toconvert);
		}
		TypeNumber::ItI16(toconvert) => {
			println!("It is ItI16 {:?}", toconvert);
		}
		TypeNumber::ItI32(toconvert) => {
			println!("It is ItI32 {:?}", toconvert);
		}
		TypeNumber::ItI64(toconvert) => {
			println!("It is ItI64 {:?}", toconvert);
		}
		TypeNumber::ItU8(toconvert) => {
			println!("It is ItU8 {:?}", toconvert);
		}
		TypeNumber::ItU16(toconvert) => {
			println!("It is ItU16 {:?}", toconvert);
		}
		TypeNumber::ItU32(toconvert) => {
			println!("It is ItU32 {:?}", toconvert);
		}
		TypeNumber::ItU64(toconvert) => {
			println!("It is ItU64 {:?}", toconvert);
		}
	}
}

fn convert_char_str(toconvert: TypeCharStr) {
	match toconvert {
		TypeCharStr::ItChar(toconvert) => {
			println!("It is ItChar {:?}", toconvert);
			print_type_of(&toconvert)
		}
		TypeCharStr::ItString(toconvert) => {
			println!("It is ItString {:?}", toconvert);
			print_type_of(&toconvert)
		}
		TypeCharStr::ItStr(toconvert) => {
			println!("It is ItStr {:?}", toconvert);
			print_type_of(&toconvert)
		}
	}
}

fn convert(x:Generic){
	match x {
		Generic::Number(n) => { convert_number(n) }
		Generic::CharStr(s)=> { convert_char_str(s) }
	}
}

fn try_convert((bases, len_bases) : (Vec<Generic>, usize)){
	for i in 0..len_bases {
		convert(bases[i].clone());
	}
}

fn main() {
	trying_mix(data());
	try_convert(data());
}