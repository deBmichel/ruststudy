struct Foo<T: Ord> {
    arr: Vec<T>,
}

fn alter() {
	let mut generic = Foo::<_> {arr: Vec::new()};
	generic.arr.push("michel");
	generic.arr.push(1);
}

fn main() {
    println!("o");

}
