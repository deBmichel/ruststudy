fn main() {
    let mut s = String::from("hello");

    s.push_str(", world!"); // push_str() appends a literal to a String

    println!("{}", s); // This will print `hello, world!`

    /*
		So, what’s the difference here? Why can String be mutated but 
		literals cannot? The difference is how these two types deal 
		with memory.
    */

    let s1 = String::from("hello");
    let mut s2 = s1.clone();
    s2.push_str(" michel");

    println!("s1 = {}\ns2 = {}", s1, s2);
    s2 = change(s2);
    println!("{:?}", s2);
}

fn change (mut s: String) -> String {
	s.push_str(" ooo add");
	s
}
