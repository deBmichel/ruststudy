fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
	fn rectangle_area (&self) -> u32 {
		self.width * self.height
	}

	fn width (&self) -> bool {
		self.width > 0
	}

	fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    let width1 = 30;
    let height1 = 50;
    let m = 3.5;

    print_type_of(&width1);
    print_type_of(&m);

    println!(
        "The area of the rectangle is {} square pixels.",
        area(width1, height1)
    );

    let rect1 = (30, 50);

    println!(
        "The area of the rectangle is {} square pixels. Method = area_two()",
        area_two(rect1)
    );

    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!(
        "The area of the rectangle is {} square pixels. Method = area_three()",
        area_three(&rect1)
    );

    println!("{:#?} , rect", rect1);

    dbg!(&rect1);

    let scale = 2;
    let rect1 = Rectangle {
        width: dbg!(30 * scale),
        height: 50,
    };

    let rect2 = Rectangle {
        width: 10,
        height: 40,
    };

    println!(
"The area of rect1 using methods is {:?}
the width is {:?}
Can rect1 hold rect2? {}",  
    	rect1.rectangle_area(),
    	rect1.width(),
    	rect1.can_hold(&rect2)
    );

    dbg!(Rectangle::square(3));

}

fn area(width: u32, height: u32) -> u32 {
    width * height
}

fn area_two(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

fn area_three(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}