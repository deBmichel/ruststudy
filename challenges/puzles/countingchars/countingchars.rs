/*
	August 11 2022: I was playing with strings chars and counting chars.

	DataStructure/s:
		Vector.
		Hash Map.

	To play use:

	rustc -o targetmichelexe charrepeated.rs && ./targetmichelexe < input.txt > output.txt &&
	rm targetmichelexe && cat input.txt && cat output.txt && rm output.txt
*/

fn main () {
	let mut cases = read_i64();
	while cases > 0 {
		let line = read_line();
		println!("{}\n{:?}", line, solve(&line));
		cases-=1;
	}
}

fn solve (str: &String) -> HashMap<char, i32> {
	let mut counter: HashMap<char, i32> = HashMap::new();
	let chars: Vec<char> = str.chars().collect();
	for c in chars { * counter.entry(c).or_insert(0) += 1; }
	return counter
}

fn read_line()-> String {
	let mut line = String::new();
	io::stdin().lock().read_line(&mut line).unwrap();
	line = line.trim_end().to_string();
	line
}

fn read_i64 () -> i64 { read_line().parse::<i64>().unwrap() }

use std::io::{self, BufRead};
use std::collections::HashMap;
