/*
	Source: https://doc.rust-lang.org/stable/book/ch08-03-hash-maps.html
	Source note: Exercise suggested at the end of the chapter.

	Exercise:
		Given a list of integers, use a vector and return the median (when sorted, 
		the value in the middle position) and mode (the value that occurs most 
		often; a hash map will be helpful here) of the list.

	DataStructure/s:
		Vector.
		Hash Map.

	Solution Notes:
		Convert iterator to vector, using split whitespace.

		I used a base code from :
			https://stackoverflow.com/questions/26536871/how-can-i-convert-a-string-of-numbers-to-an-array-or-vector-of-integers-in-rust
		But I removed the filter and the unwrap (). I thought it should run
		and the code ran :) .

		I decided imagine it was a problem of the ICPC, then, I prepared an input
		file, and writte code to read from stdio.

		I remembered the old times, I wish to go the university one more time. Sure.

	command to play from terminal:
		rustc -o targetmichelexe 13jun20221705.rs && ./targetmichelexe < input.txt
		// you can add targetmichelexe in the .gitignore
		// the -o remember and smells like ancient c, I miss the old times.
*/
use std::collections::HashMap;
use std::io::{self, BufRead};

fn main () {
	let (cases, mut index) = (read_i32(), i32::from(0));
	loop {
		let (vector_len, line_nmbrs) = (read_usize(), read_line());
		let mut numbers: Vec<i32> = 
			line_nmbrs.split_whitespace().map(|s| s.parse().unwrap()).collect();
		numbers.sort();
	
		println!("{:?}", numbers);
		determine_median(&mut numbers, vector_len);
		determine_mode(&mut numbers, vector_len);

		index+=1; if index == cases { break };
	}
}

fn determine_median (v: &mut Vec<i32>, len: usize) {
	let index = len / 2;
	let median = v[index];

	if v.len() % 2 == 0 {
		let median = (f64::from(v[index-1]) + f64::from(v[index])) / f64::from(2);
		print_median(median, true);	
	} else {
		print_median(median, false);
	}
}

fn print_median <T : std::fmt::Debug> (median : T, average: bool) {
	let mut average_note = "";
	if average {
		average_note = " using average"
	}
	println!("median [{:?}]{}", median, average_note);
}

fn determine_mode (v: &mut Vec<i32>, len: usize) {
	let mut counter: HashMap<i32, i32> = HashMap::new();
	for i in 0..len {
		// using dereference here ? , be carefull. 3:) 
		* counter.entry(v[i]).or_insert(0) += 1;
	}

	let mut modes: HashMap<i32, i32> = HashMap::new();
	let initial_key = v[0];
	let (mut changing_val, mut equal) = 
		(counter.get(&initial_key).unwrap_or(&0), true);
	modes.insert(initial_key, *changing_val);

	for (key, value) in &counter {
		if value > changing_val {
        	equal = false;
        	changing_val = value;
        	modes.clear();
        	modes.insert(*key, *value);
        }

        if value < changing_val {
        	equal = false;
        }

        if value == changing_val {
        	modes.insert(*key, *value);
        }
    }

    print_mode(equal, modes);
}

fn print_mode(equal: bool, modes: HashMap<i32, i32>){
	let quantity_modes = modes.len();
	if equal {
    	println!("No mode, all data in data set has the same frequency");
    	return
    }
	
	if quantity_modes == 1{
		println!("UNImodal MODE / OCURRENCES {:?}", modes);
		return
	}

	if quantity_modes == 2{
		println!("BImodal MODE / OCURRENCES {:?}", modes);
		return
	}

	if quantity_modes == 3{
		println!("TRImodal MODE / OCURRENCES {:?}", modes);
		return
	}

	println!("Multimodal MODE / OCURRENCES {:?}", modes);
}

fn read_line()-> String {
	let mut line = String::new();
	io::stdin().lock().read_line(&mut line).unwrap();
	line = line.trim_end().to_string();
	line
}

fn read_i32 () -> i32 {
	read_line().parse::<i32>().unwrap()
}

fn read_usize () -> usize {
	read_line().parse::<usize>().unwrap()
}
