/*
	August 10 2022: I was playing with numbers, some maths
	are incredible you can see the universe reading and
	looking numbers.

	Today I had to reduce some number to one digit using
	continous addition until get only one digit:

	12345678990012 = 1+2+3+4+5+6+7+8+9+9+0+0+1+2 ...until .. > one digit. 

	To play use:
	rustc -o targetmichelexe onedigit.rs
	./targetmichelexe < input.txt > output.txt
	rm targetmichelexe
	cat output.txt
	rm output.txt
*/

use std::io::{self, BufRead};

fn main () {
	let mut cases = read_i64();
	while cases > 0 {
		let number = read_line();
		println!("{}", reductor(&number));
		cases-=1;
	}
}


fn reductor(line: &String) -> String {
	let tem = line.clone();
	if tem.len() == 1 {
		return tem.to_string();
	} else {
		return reductor(&adder(&tem));
	}

}

fn adder(line: &String) -> String {
	let chars: Vec<char> = line.chars().collect();
	let mut sum = 0;
	for c in chars {
		if c >= '0' && c <= '9' {
			sum += c as i32 - 48;
		}
	}
	return sum.to_string();
}

fn read_line()-> String {
	let mut line = String::new();
	io::stdin().lock().read_line(&mut line).unwrap();
	line = line.trim_end().to_string();
	line
}

fn read_i64 () -> i64 { read_line().parse::<i64>().unwrap() }
