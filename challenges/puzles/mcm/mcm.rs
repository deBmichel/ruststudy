/*
	August 8 2022: I was playing with numbers, is
	incredible you can find and learn of numbers
	Today I had to find the least common multiple
	from 1 to n and I thought: I am going to writte
	a posible solution using rust.

	To play use:
	rustc -o targetmichelexe mcm.rs
	./targetmichelexe < input.txt > output.txt
	rm targetmichelexe
	cat output.txt
	rm output.txt

*/

use std::io::{self, BufRead};

fn main () {
	let mut cases = read_i32();
	loop {
		let val_to_mcm = read_usize();
		println!("{}\t{}", val_to_mcm, solve_mcm(val_to_mcm));

		cases -= 1; if cases == 0 {break}
	}
}

fn solve_mcm(n: usize) -> i64 {
	let (mut mcm, mut base, len) : (i64, i64, i64) = (1, 2, ((n as i64) + 1));
	let mut numbers_in_mcm: Vec<i64> = (base..len).collect();
	let mut factors: Vec<i64> = Vec::new();

	while base < len {
		let (mut divided, mut divisible) = divide((&mut numbers_in_mcm, base));
		if divided { factors.push(base); }
		while divisible {
			(divided, divisible) = divide((&mut numbers_in_mcm, base));
			if divided { factors.push(base); }
		}
		base += 1
	}

	for i in factors { mcm = mcm * i ;}

	mcm
}

fn divide((vector, divisor):(&mut Vec<i64>, i64)) -> (bool, bool) {
	let (mut divided, mut divisible) = (false, false);
	for i in vector {
		if *i % divisor == 0 {
			divided = true;
			*i = *i / divisor;
			if *i % divisor == 0 {
				divisible = true;
			}
		}
	}
	(divided, divisible)
}

fn read_line()-> String {
	let mut line = String::new();
	io::stdin().lock().read_line(&mut line).unwrap();
	line = line.trim_end().to_string();
	line
}

fn read_i32 () -> i32 {
	read_line().parse::<i32>().unwrap()
}

fn read_usize () -> usize {
	read_line().parse::<usize>().unwrap()
}
