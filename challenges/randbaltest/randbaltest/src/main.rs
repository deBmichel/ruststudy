use rand::{Rng};
use std::io::{self, BufRead};

fn read_line()-> String {
	let mut line = String::new();
	io::stdin().lock().read_line(&mut line).unwrap();
	line = line.trim_end().to_string();
	line
}

fn verify_element ((from, element) : (&Vec<String>, &String)) -> bool {
	for i in from {
		if *i == *element {
			return true
		}
	}
	return false
}

fn selector ((quantity, from) : (i32, &mut Vec<String>)) -> Vec<String>{
	let mut returned: Vec<String> = Vec::new();
	let mut rng = rand::thread_rng();
	for _ in 0..quantity {
		let selected = rng.gen_range(0..from.len()) as usize;
		returned.push(from[selected].clone());
		from.remove(selected);
	}
	returned
}

fn main() {
	let mut strset: Vec<String> = Vec::new();
	let mut line = read_line();
	loop {
		if line == "" { break }

		if !verify_element((&strset, &line)){
			strset.push(line);
		}

		line = read_line();
	}

	let mut strfin: Vec<String> = Vec::new();
	for i in 0..17 {
		strfin.push(i.to_string())
	}

	println!("Enter rounds !: ??");
	let num: i32 = read_line().parse().unwrap();
	println!("hi.", );
	let mut finished: Vec<i32> = Vec::new();

	for k in 0..num {
		let mut changer = strset.clone();
		let mut fin = strfin.clone();	

		let selected = selector((5, &mut changer));
		let finalo = selector((1, &mut fin))[0].clone();
		
		let mut numbers: Vec<i32> = Vec::new();
		for s in selected {
			numbers.push(s.parse().unwrap());
		}

		numbers.sort();
		numbers.push(finalo.parse().unwrap());
		finished = numbers.clone();
		println!("{:?}", k);
	}
	println!("{:?}", finished);
}
