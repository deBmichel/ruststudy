use std::io::{self, BufRead};
use std::collections::HashMap;

fn read_line()-> String {
	let mut line = String::new();
	io::stdin().lock().read_line(&mut line).unwrap();
	line = line.trim_end().to_string();
	line
}

fn reductor(line: &String) -> String {
	let tem = line.clone();
	if tem.len() == 1 {
		return tem.to_string();	
	} else {
		return reductor(&adder(&tem));
	}
	
}

fn adder(line: &String) -> String {
	let chars: Vec<char> = line.chars().collect();
	let mut sum = 0;
	for c in chars {
		if c >= '0' && c <= '9' {
			sum += c as i32 - 48;
		}
	}
	return sum.to_string();
}

fn get_most_common(from : Vec<String>){
	let mut frecuency: HashMap<String, u32> = HashMap::new();
	for i in from {
		*frecuency.entry(i).or_insert(0) += 1;
	}
}

fn main(){
	let mut line = read_line();
	loop {
		if line == "" { break }

		println!("{} \t{} \t{}", line, adder(&line), reductor(&line));

		line = read_line();
	}
}
