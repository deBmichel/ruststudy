<div align="center">
<h1 align="center">1: Cargo Basic</h1>
Hello !, cordial greeting for you.<br />
Tutorial Date: Mon Jan 22 04:22:01 PM -05 2024<br />
<br />
</div>

In this tutorial we are going to play with cargo, and we are going to experiment with some things, aspects or elements in cargo.<br /><br />

<div align="center">
<h2 align="center">What is cargo ?</h2> 
</div>
Easy answers:<br />

cargo is the package manager and crate host for rust.<br />
&nbsp;&nbsp;&nbsp;&nbsp;taken from: [https://crates.io/](https://crates.io/) 

Cargo is the Rust package manager. Cargo downloads your Rust package’s dependencies, compiles your packages, makes distributable packages, and uploads them to crates.io, the Rust community’s package registry. <br />
&nbsp;&nbsp;&nbsp;&nbsp;taken from: [https://doc.rust-lang.org/cargo/](https://doc.rust-lang.org/cargo/) 

## Before starting:

In this tutorial you will learn basic things about cargo and you will get an idea of ​​how to execute a project with cargo. In this tutorial we will not publish to crates.io. You will learn basic things but you will not become a Cargo professional.<br />

We are going to be using Debian Operating system for this tutorial. You can try follow the instructions in your operating system.<br /> 

You need to install cargo and rust in your operating system.

If you are in a mobile you can use zoom for images, or try touching the image two times.!!!

## What we will do in this tutorial ?

We are going to use Cargo Workspaces and we are going to do a microservices simulation; we will expose the microservices simulation using actix and rocket and we will have two binaries to run our microservices.

<div align="center">
<h2 align="center">Tutorial 1: Cargo Basic</h2> 
</div>

The first Step: is review cargo and rustc versions, to review, in the terminal we are going to execute the command: (Review image, look for date)
```bash
cargo --version && rustc --version && date
```
<br />
<div align="center">
<img src="images/1.png" alt="Image 1"/>
</div>
<br />
Now before starting to use Rust and Cargo we are going to define the hierarchy of some folders for our project, initially I thought of something like: (Initial Hierarchy)

```bash
├── 1:CargoBasic        -----> (The main folder, contains all)
│   ├── db              -----> (Related with db manager) 
│   ├── exposers        -----> (Related with actix and rocket)
│   ├── main            -----> (Related with two mains or binaries to run the project)
│   ├── microservices   -----> (Contains some microservices simulations, basic code)
│   ├── Cargo.toml      -----> The Cargo .toml file for Cargo.
│   └── README.md       -----> The Readme file for 1:CargoBasic
```
<br />
Step 2: Creating the folder 1:CargoBasic, README.md and Cargo.toml: We are going to execute the commands:

```bash
mkdir 1:CargoBasic
cd 1:CargoBasic
touch Cargo.toml
pwd
tree -a
touch README.md
tree -a
```
<br />
<div align="center">
<img src="images/2.png" alt="Image 2"/>
</div>
<br />

Step 3: Editing Cargo.toml, using workspaces and first build.
If you read [https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html](https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html) , you can find some additional and important information; Here we are going to start editing our Cargo.toml file with some text editor and we add our workspace members, remember we are using the Initial Hierarchy, then:

1: We start adding library db: 

Edit the Cargo.toml file adding the next lines and saving the changes in the file:

```bash
[workspace]
members = [
	"db"
]
```

now in the terminal execute the next command:

```bash
cat Cargo.toml
cargo new db --lib
tree -a
```

you should find something like:
<div align="center">
<img src="images/3.png" alt="Image 3"/>
</div>
<br />

We have some warnings, the first is because the main folder has the char (:)->(1:CargoBasic), at this point it is not important, the second is related with the resolver, you can read [https://doc.rust-lang.org/cargo/reference/resolver.html#resolver-versions
](https://doc.rust-lang.org/cargo/reference/resolver.html#resolver-versions), to solve the resolver warning we are going to modify the Cargo.toml file to: (and we save the chages in the file)

```bash
[workspace]
members = [
	"db"
]
resolver = "2"
```

2: We continue adding the library exposers:

Modify the Cargo.toml file to : (saving the changes in the file)
```bash
[workspace]
members = [
	"db",
    "exposers"
]
resolver = "2"
```

now in the terminal execute the next command:

```bash
cat Cargo.toml
cargo new exposers --lib
tree -a
```

you should find something like:
<div align="center">
<img src="images/4.png" alt="Image 4"/><br />
<img src="images/5.png" alt="Image 5"/>
</div>
<br />

If you review with calm you can see only ONE warning related with char(:), the resolver warning now is not visible. Review and verify you have two new folders : db and exposers, each folder has Cargo.toml and lib.rs (we will use those files later).

3: We continue adding the library microservices:

Modify the Cargo.toml file to : (saving the changes in the file)
```bash
[workspace]
members = [
    "db",
    "exposers",
    "microservices"
]
resolver = "2"
```

now in the terminal execute the next command:

```bash
cat Cargo.toml
cargo new microservices --lib
tree -a
```

you should find something like:
<div align="center">
<img src="images/6.png" alt="Image 6"/>
</div>
<br />

you can see only ONE warning related with char(:) !!!

4: We add the binary lib main, to have two binaries one related with actix, second related with rocket. 

Modify the Cargo.toml file to : (saving the changes in the file)
```bash
[workspace]
members = [
    "db",
    "exposers",
    "microservices",
    "main"
]
resolver = "2"
```

now in the terminal execute the next command:

```bash
cat Cargo.toml
cargo new main
tree -a
```

you should find something like:
<div align="center">
<img src="images/7.png" alt="Image 7"/>
</div>
<br />

IN THIS CASE we did not use --lib arg for cargo, and in the terminal you can read  Created binary (application) `main` package, `binary` is diferent from `library`. At this point we have 4 folders created using cargo command.

Note: At this point if you run the tree command you should see something like this:

pending image

`Note: here we do not have the target folder, the target folder is created by charge and you should consider putting the target folder in the .gitignore of a git project.`

5: Is the time to execute :
```bash
cargo build
```
you should find something like:
<div align="center">
<img src="images/8.png" alt="Image 8"/>
</div>
<br />

We find an error related with : path segment contains separator `:` ,to solve this we are going to rename `1:CargoBasic` to `1CargoBasic` without `:` and reexecute `cargo build` executing:

```bash
cd ..
mv 1\:CargoBasic/ 1CargoBasic/
cd 1CargoBasic/
cargo build
```

you should find something like:
<div align="center">
<img src="images/9.png" alt="Image 9"/>
</div>
<br />

Lesson: characters in folder names are important, be carefull with the name of your folders, cargo is sensible to special chars in paths or folder names.

If you review the image all 4 folders we created were compiled "db", "exposers", "microservices" and "main".

And now if you review you can find the `target folder` created by cargo. Be carefull.

`A workspace is a collection of one or more packages, called workspace members, that are managed together.`

You can read about workspaces in : [https://doc.rust-lang.org/cargo/reference/workspaces.html](https://doc.rust-lang.org/cargo/reference/workspaces.html)

Workspaces is a concept that allows you to organize and divide a project in folders, but Workspaces in Cargo have more features.

You have code and tests created using cargo, you can review the project and the files created using cargo.

the command `cargo build` builds all the project, and prepare binaries to run, you can execute the command:

```bash
cargo run
```

you will wee the output "Hello, World!", but you did not write the code, you can review the main folder,
and you will find the main.rs file, but you can have multiple binaries and multiple project runners, we will create two diferent binaries in some step later.

the command `cargo run` will search the binary with a main function if you have more than one binarie cargo will tell you.

At this point you can execute the command :

```bash
cargo test
```

you will find test you did not writte, one more time cargo created code, basic and functional code.

Step 3 is finished !!!!

Step 4: Playing to be microservices: the concept microservice, is complex(you can find good books and teory about microservices in internet or with a software engineer), in this case we are going to imagine a microservice like a simple function exposed vía some web technology or framework (actix and rocket in the tutorial).

1: Imagine: we can have microservices of all types, and of all subjects; in the library microservices we can create all microservices we wish, in this case each microservice can be one simple folder in the library microservice, obvious we can imagine an aceptable idea of microservice with tests and some of simple code (emulating interesting good practices like tests).

2: Starting with a simple {add microservice} : {add microservice} is simple, we have one microservice receiving two parameters and returning the sum of the parameters. To create the {add microservice} we are going to use the next structure: (read with calm)

```bash
├── add                    -----------> microservice folder.
│   ├── model.rs           -----------> module to give form to the service.
│   ├── modeltest.rs       -----------> to test model.rs
│   ├── mod.rs             -----------> It is an important file for Cargo: to declare all modules.
│   ├── service.rs         -----------> to define the main function of the service.
│   ├── servicetest.rs     -----------> to test service.rs
│   ├── validators.rs      -----------> only to validate the parameters in the microservice.
│   └── validatorstest.rs  -----------> to test validators.rs
```

`Where to create the folder add ????` To create the folder add we are going to use the library `microservices` we created in `step 3`, path = microservices/src/(here we create the add folder).

It is the first microservice simulation and you can find all the code in: [https://gitlab.com/deBmichel/ruststudy/-/tree/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add](https://gitlab.com/deBmichel/ruststudy/-/tree/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add)

if your terminal is ubicated in  folder 1CargoBasic/ you can create the code using the next command:

```bash
cd microservices/src/

mkdir add

cd add

wget https://gitlab.com/deBmichel/ruststudy/-/raw/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add/mod.rs 

wget https://gitlab.com/deBmichel/ruststudy/-/raw/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add/model.rs 

wget https://gitlab.com/deBmichel/ruststudy/-/raw/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add/modeltest.rs 

wget https://gitlab.com/deBmichel/ruststudy/-/raw/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add/service.rs 

wget https://gitlab.com/deBmichel/ruststudy/-/raw/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add/servicetest.rs 

wget https://gitlab.com/deBmichel/ruststudy/-/raw/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add/validators.rs 

wget https://gitlab.com/deBmichel/ruststudy/-/raw/main/tutorials/tutorialscode/1CargoBasic/microservices/src/add/validatorstest.rs 	
	
cd .. && cd .. && cd ..

```



the code for add is not complex, is really simple, one important file here is:

microservices/src/add/mod.rs    -> read the code of mod.rs

in mod.rs we declare all modules of the add microservice folder, cargo search the file mod.rs.

other importan file to review is : 

microservices/src/add/lib.rs    -> read the code of lib.rs.

in lib.rs we define mod add, and cargo will search the file add/mod.rs

if you are curious, you can try create a simple micro service simulation like a hello world function and cargo will tell you what do you have to do, you can start creating a new folder named helloworld in microservices/src folder.

## Minimal experimentation with tests::

we have the next tests for add:

test add::modeltest::test_basic_model 
test add::modeltest::test_functional_simple_add 
test add::servicetest::test_basic_model_from_service 
test add::modeltest::test_failing_num_str 
test add::validatorstest::test_invalid_float 
test add::servicetest::test_failing_num_str_from_service 
test add::servicetest::test_functional_simple_add_from_service 
test add::validatorstest::test_valid_float 
test add::servicetest::test_decimal_from_service 

only simple tests but obvious you can create complex tests or integration test, review the files: validatorstest.rs servicetest.rs modeltest.rs

to obtain the list execute:
cargo test --package=microservices

we are going to experiment here:

we run all test of microservices showing in terminal the outputs of println!: 
cargo test --package=microservices -- --nocapture

cargo test --package=microservices -- --show-output

we run all test of microservices without the outputs of println!:
cargo test --package=microservices

we can run tests of microservices using the command:
cargo test -p microservices 

in this case we are going to execute the specific test add::validatorstest::test_invalid_float

to run the specific test you can use :
cargo test --package=microservices add::validatorstest::test_invalid_float -- --exact --nocapture

cargo test --package=microservices test_invalid_float -- --nocapture

cargo test --package=microservices add::validatorstest::test_invalid_float -- --exact --show-output

cargo test --package=microservices add::validatorstest::test_invalid_float -- --exact 

cargo test -p microservices add::validatorstest::test_invalid_float -- --exact

cargo test -p microservices add::validatorstest::test_invalid_float -- --exact --nocapture

now we are going to execute the test add::servicetest::test_functional_simple_add_from_service 
we can use :

cargo test --package=microservices add::servicetest::test_functional_simple_add_from_service -- --exact --nocapture

cargo test --package=microservices test_functional_simple_add_from_service -- --nocapture

cargo test --package=microservices add::servicetest::test_functional_simple_add_from_service -- --exact 

cargo test -p microservices add::servicetest::test_functional_simple_add_from_service -- --exact

cargo test -p microservices add::servicetest::test_functional_simple_add_from_service -- --exact --nocapture

you can experiment like you wish and you can read [https://doc.rust-lang.org/book/ch11-02-running-tests.html](https://doc.rust-lang.org/book/ch11-02-running-tests.html)

if you review with calm you can find diferent outputs depending of the argument cargo test. 










you should find something like:
<div align="center">
<img src="images/.png" alt="Image "/><br />
<img src="images/.png" alt="Image "/>
</div>
<br />

&nbsp;&nbsp;&nbsp;&nbsp;taken from: [https://crates.io/](https://crates.io/) 


# acknowledgment and thanks:

In creating this tutorial I want to thank my friend Sthepha, she is a good engineer and lent me a laptop to experiment with while creating this tutorial.

She is a good friend.


