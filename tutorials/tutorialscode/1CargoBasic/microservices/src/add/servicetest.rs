use crate::add::model;
use crate::add::service;

#[test]
fn test_basic_model_from_service() {
    let a = "michel".to_string();
    let b = "m s".to_string();
    let to_compare = model::Answer {
        status: "Add was not executed: michel IS NOT A NUMBER. m s IS NOT A NUMBER. ".to_string(),
        result: "michel + m s".to_string()
    };

    let result = service::add_service(a,b);
    println!("obtained {:#?}", result);

    assert_eq!(result, to_compare);
}

#[test]
fn test_functional_simple_add_from_service() {
    let a = "764589".to_string();
    let b = "945783".to_string();
    let to_compare = model::Answer {
        status: "Add was executed".to_string(),
        result: "1710372".to_string()
    };

    let result = service::add_service(a,b);
    println!("obtained {:#?}", result);

    assert_eq!(result, to_compare);
}

#[test]
fn test_failing_num_str_from_service() {
    let a = "764589".to_string();
    let b = "michel".to_string();
    let to_compare = model::Answer {
        status: "Add was not executed: michel IS NOT A NUMBER. ".to_string(),
        result: "764589 + michel".to_string()
    };

    let result = service::add_service(a,b);
    println!("obtained {:#?}", result);

    assert_eq!(result, to_compare);
}

#[test]
fn test_decimal_from_service() {
    let a = "-3.5".to_string();
    let b = "-3.5".to_string();
    let to_compare = model::Answer {
        status: "Add was executed".to_string(),
        result: "-7".to_string()
    };

    let result = service::add_service(a,b);
    println!("obtained {:#?}", result);

    assert_eq!(result, to_compare);
}

