use crate::add::validators;

#[derive(Debug)]
#[derive(PartialEq)]
pub struct Answer {
    pub status: String,
    pub result: String,
}

pub fn take_new_add(a:String, b:String) -> Answer{
    let mut status = String::from("");
    
    let (fa, va) = validators::validatefloat(a.clone());
    if !fa {
        status = status + &a.clone() + " IS NOT A NUMBER. ";
    }
    let (fb, vb) = validators::validatefloat(b.clone());
    if !fb {
        status = status + &b.clone() + " IS NOT A NUMBER. ";
    }

    if fa && fb {
        return Answer {
                    status:String::from("Add was executed"), 
                    result:(va+vb).to_string()
                }
        ;
    }

    return Answer {
        status: "Add was not executed: ".to_owned()+&status, 
        result: a.clone()+" + "+&b.clone()
    };
    
}

