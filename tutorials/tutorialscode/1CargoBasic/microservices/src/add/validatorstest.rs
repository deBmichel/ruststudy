use crate::add::validators;
#[test]
fn test_valid_float() {
    println!("running test_valid_float");
    let v = "-3.678".to_string();
    let (f, val) = validators::validatefloat(v);
    assert_eq!((f, val), (true, -3.678));
}

#[test]
fn test_invalid_float() {
    println!("running test_invalid_float");
    let v = "-3.x".to_string();
    let (f, val) = validators::validatefloat(v);
    assert_eq!((f, val), (false, -1.0));
}
