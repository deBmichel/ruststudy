use crate::add::model;

#[test]
fn test_basic_model() {
    let a = "michel".to_string();
    let b = "m s".to_string();
    let result = model::take_new_add(a,b);
    println!("obtained {:#?}", result);
    let to_compare = model::Answer {
        status: "Add was not executed: michel IS NOT A NUMBER. m s IS NOT A NUMBER. ".to_string(),
        result: "michel + m s".to_string()
    };
    assert_eq!(result, to_compare);
}

#[test]
fn test_functional_simple_add() {
    let a = "764589".to_string();
    let b = "945783".to_string();
    let result = model::take_new_add(a,b);
    println!("obtained {:#?}", result);
    let to_compare = model::Answer {
        status: "Add was executed".to_string(),
        result: "1710372".to_string()
    };
    assert_eq!(result, to_compare);
}

#[test]
fn test_failing_num_str() {
    let a = "764589".to_string();
    let b = "michel".to_string();
    let result = model::take_new_add(a,b);
    println!("obtained {:#?}", result);
    let to_compare = model::Answer {
        status: "Add was not executed: michel IS NOT A NUMBER. ".to_string(),
        result: "764589 + michel".to_string()
    };
    assert_eq!(result, to_compare);
}

/*
#[test]
fn test_alg_empthy() {
    use crate::algorithms::algorithms;
    let result = algorithms::ret_empthy();
    assert_eq!(result, true);
}

#[test]
fn string_base64_enc_dec() {
    use crate::algorithms::algorithms;
    let strt = "michel".to_string();
    let enc_ans = algorithms::base64_enc(strt.clone());
    let dec_ans = algorithms::base64_dec(enc_ans);
    assert_eq!(strt.clone(), dec_ans);
}

#[test]
fn rawjson_base64_enc_dec(){
    use crate::algorithms::algorithms;
    let data = r#"
    {
        "name": "John Doe",
        "age": 43,
        "phones": [
          "+44 1234567",
          "+44 2345678"
        ]
    }"#.to_string();
    let enc_ans = algorithms::base64_enc(data.clone());
    let dec_ans = algorithms::base64_dec(enc_ans);
    assert_eq!(data.clone(), dec_ans);
}

#[test]
fn string_aes_enc_dec() {
    use crate::algorithms::algorithms;
    let strt = "aes".to_string();
    let enc_ans = algorithms::aes_enc(strt.clone());
    let dec_ans = algorithms::aes_dec(enc_ans);
    assert_eq!(strt.clone(), dec_ans);
}

#[test]
fn rawjson_aes_enc_dec(){
    use crate::algorithms::algorithms;
    let data = r#"
    {
        "name": "John Doe",
        "age": 43,
        "phones": [
          "+44 1234567",
          "+44 2345678"
        ]
    }"#.to_string();
    let enc_ans = algorithms::aes_enc(data.clone());
    let dec_ans = algorithms::aes_dec(enc_ans);
    assert_eq!(data.clone(), dec_ans);
}


*/


