pub fn validatefloat(pif: String)-> (bool, f64){
    match pif.parse::<f64>() {
        Ok(n) => return (true, n),
        Err(_) => return (false, -1.0)
    }
}

